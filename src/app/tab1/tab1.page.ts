
import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public datax : Array<any> = [];
  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit() {
    // this.getdata();
    this.rest.business().subscribe(res => {
      this.datax = res;
      console.log(this.datax);
    });
  }

}
