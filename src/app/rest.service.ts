import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RestService {

 
  public api = "http://locator.movablebrains.com/wp-json/wp/v2/";
  constructor(private http: HttpClient) { }

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  

  //get data
  business(): Observable<any> {
    const apiurl = this.api+'panciteria';
    console.log('apiurl: '+apiurl);
    return this.http.get(apiurl).pipe(
      map(this.extractData));
  }

 
  //data view
  businessid(id: string): Observable<any> {
    const apiurl = this.api+'panciteria/'+id;
    console.log('apiurl: '+apiurl);
    return this.http.get(apiurl).pipe(
      map(this.extractData));
  }

}
