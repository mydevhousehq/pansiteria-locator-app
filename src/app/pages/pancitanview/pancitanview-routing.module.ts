import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PancitanviewPage } from './pancitanview.page';

const routes: Routes = [
  {
    path: '',
    component: PancitanviewPage
  },{
    path: ':id',
    component: PancitanviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PancitanviewPageRoutingModule {}
