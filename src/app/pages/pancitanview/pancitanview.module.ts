import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PancitanviewPageRoutingModule } from './pancitanview-routing.module';

import { PancitanviewPage } from './pancitanview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PancitanviewPageRoutingModule
  ],
  declarations: [PancitanviewPage]
})
export class PancitanviewPageModule {}
